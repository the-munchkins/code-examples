from unittest import TestCase
from unittest.mock import patch

from main import get_sum_two_random_number


class Test(TestCase):

    @patch('main.random')   # notice that it is "main.random", and not "random.random". One needs to mock the function random() used in main.py
    def test_if_random_patched_via_decorator_returns_zero_then_sum_is_zero(self, mocked_random):
        mocked_random.return_value = 0
        self.assertEqual(0, get_sum_two_random_number())

    def test_if_random_patched_via_with_statement_returns_zero_then_sum_is_zero(self):
        with patch('main.random') as mocked_random:
            mocked_random.return_value = 0
            self.assertEqual(0, get_sum_two_random_number())
